(ns euler.util)

(defn factor? [a b]
  (zero? (mod a b)))

(defn small-factors [n]
  (concat (filter #(factor? n %)
                  (range 1 (Math/sqrt n)))))

(defn factors [n]
  (let [fs (small-factors n)]
    (sort
     (distinct
      (into (map #(/ n %) fs) fs)))))

(defn proper-factors [n]
  (remove (partial == n) (factors n)))

(defn divisor-fn [n]
  (reduce + (proper-factors n)))

(def divisor-fn-memo
  (memoize divisor-fn))

(defn factorial [n]
  (reduce * (range
             (bigint 1)
             (bigint (inc n)))))

(defn choose [n k]
  (/ (factorial n)
     (* (factorial k)
        (factorial (- n
                      k)))))

(def fibs
  ((fn rfib [a b]
     (lazy-seq (cons a (rfib b (+ a b)))))
   (bigint 0) (bigint 1)))

(defn gcd [a b]
  (cond (zero? b) a
        :else (recur b (mod a b))))

(defn lcm [a b]
  (/ (* a b)
     (gcd a b)))

(defn sum-of-digits [x]
  ((fn r-sum [x total]
     (cond (< x 10) (+ total x)
           :else
           (r-sum (quot x 10)
                  (+ (rem x 10) total))))
   x 0))

(defn exp [a n]
  (reduce * (repeat n (bigint a))))

(defn int-reverse [n]
  (defn reverse-it [reversed n]
    (cond (= n 0) reversed
          :else (reverse-it (+ (* 10 reversed) (mod n 10)) (quot n 10))))
  (reverse-it 0 n))

(defn int-palindrome? [n]
  (= (int-reverse n) n))

(defn prime-c? [n certainty]
  (.isProbablePrime (BigInteger/valueOf n) certainty))

(defn prime? [n]
  (prime-c? n 7))

(def primes
  (filter prime?
          (cons 2 (range 3 Integer/MAX_VALUE 2))))

(def triangles
  ((fn rtri [n]
    (lazy-seq (cons (/ (* n (+ n 1)) 2)
                    (rtri (inc n)))))
  1))

(defn groupify [xs n]
  (cond (<= (count xs) n) (list xs)
        :else (concat (list (take n xs)) (groupify (rest xs) n))))

(defn transpose [m]
  (apply mapv vector m))
