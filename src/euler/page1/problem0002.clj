(ns euler.page1.problem0002
  (:require [euler.util :refer [fibs]]))

(def problem0002
  (reduce +
          (filter even?
                  (take-while #(< % 4000000)
                              fibs))))
