(ns euler.page1.problem0028)

(defn steps [step]
  (lazy-cat (repeat 4 step) (lazy-seq (steps (+ step 2)))))

(def spiral-size 1001)

(def num-steps (inc (* (quot spiral-size 2) 4)))

(def problem0028
  (reduce +
          (cons 1 (map #(inc (reduce + (take % (steps 2))))
                       (range 1 num-steps)))))
