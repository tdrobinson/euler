(ns euler.page1.problem0030
  (:require [euler.util :refer [exp]]))

(defn int-power-sum [n power]
  ((fn int-power-sum' [n power total]
     (cond (< n 10) (+ total (exp n power))
           :else (int-power-sum' (quot n 10) power (+ total (exp (mod n 10) power)))))
   n power 0))

(def power 5)

(def problem0030
  (reduce +
          (map first
               (filter (fn [ns]
                         (== (first ns) (second ns)))
                       (map
                        #(list % (int-power-sum % power))
                        (range 10 (int-power-sum 999999 power)))))))
