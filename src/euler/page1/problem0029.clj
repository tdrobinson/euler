(ns euler.page1.problem0029
  (:require [euler.util :refer [exp]]))

(def upper-bound 100)

(def problem0029
  (count
   (set
    (mapcat (fn [a]
              (map (fn [b]
                     (exp a b))
                   (range 2 (inc upper-bound))))
            (range 2 (inc upper-bound))))))
