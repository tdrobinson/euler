(ns euler.page1.problem0007
  (:require [euler.util :refer [primes]]))

(def problem0007
  (nth primes (dec 10001)))
