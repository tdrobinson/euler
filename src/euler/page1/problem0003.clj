(ns euler.page1.problem0003
  (:require [euler.util :refer [prime? factors]]))

(def problem0003
  (last
   (filter #(prime? %)
           (factors 600851475143))))
