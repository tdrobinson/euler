(ns euler.page1.problem0017)

(defn number-to-words [n]
  (cond (== n 0) ""
        (== n 1) "one"
        (== n 2) "two"
        (== n 3) "three"
        (== n 4) "four"
        (== n 5) "five"
        (== n 6) "six"
        (== n 7) "seven"
        (== n 8) "eight"
        (== n 9) "nine"
        (== n 10) "ten"
        (== n 11) "eleven"
        (== n 12) "twelve"
        (== n 13) "thirteen"
        (== n 15) "fifteen"
        (== n 18) "eighteen"
        (< n 20) (str (number-to-words (rem n 10)) "teen")
        (== n 20) "twenty"
        (== n 30) "thirty"
        (== n 40) "forty"
        (== n 50) "fifty"
        (== n 80) "eighty"
        (and (< n 100) (zero? (mod n 10))) (str (number-to-words (quot n 10)) "ty")
        (< n 100) (str (number-to-words (* (quot n 10) 10)) (number-to-words (rem n 10)))
        (and (< n 1000) (zero? (mod n 100))) (str (number-to-words (quot n 100)) "hundred")
        (< n 1000) (str (number-to-words (* (quot n 100) 100)) "and" (number-to-words (rem n 100)))
        (== n 1000) "onethousand"
        :else ""))

(def problem0017
  (reduce +
    (map #(count (number-to-words %))
       (range 1 (inc 1000)))))
