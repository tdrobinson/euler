(ns euler.page1.problem0021
  (:require [euler.util :refer [divisor-fn-memo]]))

(defn amicable? [n]
  (let [m (divisor-fn-memo n)]
    (and (not (== m n )) (== n (divisor-fn-memo m)))))

(def problem0021
  (reduce +
    (filter amicable?
            (range 2 10000))))
