(ns euler.page1.problem0025
 (:require [euler.util :refer [fibs]]))

(defn num-digits [n]
  (count (str n)))

(def problem0025
  (count
   (take-while #(< (num-digits %) 1000)
               fibs)))
