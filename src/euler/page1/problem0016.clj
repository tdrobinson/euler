(ns euler.page1.problem0016
  (:require [euler.util :refer [exp sum-of-digits]]))

(def problem0016
  (sum-of-digits (exp 2 1000)))
