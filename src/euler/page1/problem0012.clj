(ns euler.page1.problem0012
  (:require [euler.util :refer [triangles factors]]))

(def problem0012
  (first
   (filter #(> (count (factors %))
               500)
           triangles)))
