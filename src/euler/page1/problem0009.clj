(ns euler.page1.problem0009)

(def limit 1000)

(def problem0009
  (bigint
   (apply * (first
             (filter (fn [xs]
                       (== (apply + xs) limit))
                     (for
                       [a (range 1 limit) b (range a limit)]
                       [ a b (Math/sqrt (+ (* a a) (* b b))) ]))))))
