(ns euler.page1.problem0022)

(def names
  (sort
   (clojure.string/split
    (clojure.string/replace
     (slurp
      (clojure.java.io/resource "euler/page1/names.txt"))
     #"\"" "")
    #",")))

(defn char-to-alpha-pos [c]
  (- (int c) 64))

(defn name-score [i n]
  (* (inc i) (reduce + (map char-to-alpha-pos n))))

(def problem0022
  (reduce +
          (map-indexed name-score names)))
