(ns euler.page1.problem0005
  (:require [euler.util :refer [lcm]]))

(def problem0005
  (reduce #(lcm %1 %2) (range 1 (inc 20))))
