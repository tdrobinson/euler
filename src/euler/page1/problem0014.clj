(ns euler.page1.problem0014)

(def collatz-seq-length
  (fn [n]
    (cond
     (= n 1)   1
     (even? n) (inc (collatz-seq-length (/ n 2)))
     (odd? n)  (inc (collatz-seq-length (inc (* 3 n)))))))

(defn max-pair [a b]
  (cond (> (last a) (last b)) a
        :else b))

(def problem0014
  (first
   (reduce max-pair
           (map (fn [n]
                  (list n (collatz-seq-length n)))
                (range 1 1000000)))))
