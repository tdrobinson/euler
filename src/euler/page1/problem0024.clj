(ns euler.page1.problem0024
  (:require [clojure.math.combinatorics :refer [permutations]]))

(defn list-to-num [xs]
  ((fn list-to-num' [xs total]
    (cond (empty? xs) total
          :else (list-to-num' (rest xs) (+ (* total 10) (first xs)))))
   xs 0))

(def problem0024
  (list-to-num
   (nth
    (permutations (range 0 10))
    (dec 1000000))))
