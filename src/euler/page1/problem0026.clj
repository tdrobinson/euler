(ns euler.page1.problem0026
  (:require [euler.util :refer [exp gcd]]))

(defn repeat-period [n]
  (first
   (for [s (range 0 n)
         t (range 1 n)
         :when (== (exp 10 s) (mod (exp 10 (+ s t)) n))]
     t)))


(def problem0026
  (first
   (apply max-key second
          (map (fn [n]
                 [n (or (repeat-period n) 0)])
               (filter #(== (gcd 10 %) 1)
                       (range 0 1000))))))
