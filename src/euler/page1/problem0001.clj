(ns euler.page1.problem0001)

(def problem0001
  (reduce +
          (filter #(or
                    (zero? (mod % 3))
                    (zero? (mod % 5)))
                  (range 1 1000))))
