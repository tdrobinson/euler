(ns euler.page1.problem0027
  (:require [euler.util :refer [primes prime?]]))

(def bs (take-while (partial > 1000) primes))

(def quadratic-pairs
  (mapcat (fn [b]
         (map (fn [a]
              [a b])
         (range (- b) 1000)))
       bs))

(defn quadratic [a b n]
  (+ (+ (* n n) (* n a)) b))

(defn consecutive-primes [a b]
  ((fn consecutive-primes' [a b n]
     (let [q (quadratic a b n)]
       (cond (prime? q) (consecutive-primes' a b (inc n))
             :else n)))
   a b 0))

(def problem0027
  (let [triple (apply max-key first
                      (map #(cons (consecutive-primes (first %) (second %)) %)
                           quadratic-pairs))]
    (* (nth triple 1) (nth triple 2))))
