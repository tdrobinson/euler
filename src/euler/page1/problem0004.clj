(ns euler.page1.problem0004
  (:require [euler.util :refer [int-palindrome?]]))

(def problem0004
  (apply max
         (filter int-palindrome?
                 (flatten
                  (map (fn [x]
                         (map (partial * x)
                              (range x 1000)))
                       (range 1 1000))))))
