(ns euler.page1.problem0020
  (:require [euler.util :refer [factorial sum-of-digits]]))

(def problem0020
  (sum-of-digits
   (factorial 100)))

problem0020
