(ns euler.page1.problem0010
  (:require [euler.util :refer [primes]]))

(def problem0010
  (apply +
    (take-while #(< % 2000000) primes)))
