(ns euler.page1.problem0019)

(import 'java.util.Calendar)

(def problem0019
  (count
   (filter #(== (Calendar/SUNDAY) %)
           (mapcat (fn [year]
                     (map (fn [month]
                            (let [cal (Calendar/getInstance)]
                              (.set cal year month 1)
                              (.get cal Calendar/DAY_OF_WEEK)))
                          (range 0 12)))
                   (range 1901 2001)))))
