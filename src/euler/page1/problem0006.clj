(ns euler.page1.problem0006)

(def limit 100)

(def sum (/ (* limit (inc limit)) 2))

(def sum-sq (/ (*
                (+ (* 2 limit) 1)
                (+ limit 1)
                limit)
               6))

(def problem0006 (- (* sum sum) sum-sq))
