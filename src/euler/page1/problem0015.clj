(ns euler.page1.problem0015
  (:require [euler.util :refer [choose]]))

(def problem0015
  (choose 40 20))
