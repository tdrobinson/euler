(ns euler.page2.problem0067-test
  (:require [clojure.test :refer :all]
            [euler.page2.problem0067 :refer :all]))

(deftest problem0067-test
  (testing "Problem 0067 - Maximum path sum II"
    (is (== problem0067 7273))))
