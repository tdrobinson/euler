(ns euler.page1.problem0004-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0004 :refer :all]))

(deftest problem0004-test
  (testing "Problem 0004 - Largest palindrome product"
    (is (== problem0004 906609))))
