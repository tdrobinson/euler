(ns euler.page1.problem0014-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0014 :refer :all]))

(deftest problem0014-test
  (testing "Problem 0014 - Longest Collatz sequence"
    (is (== problem0014 837799))))
