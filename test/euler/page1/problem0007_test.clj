(ns euler.page1.problem0007-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0007 :refer :all]))

(deftest problem0007-test
  (testing "Problem 0007 - 10001st prime"
    (is (== problem0007 104743))))
