(ns euler.page1.problem0028-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0028 :refer :all]))

(deftest problem0028-test
  (testing "Problem 0028 - Number spiral diagonals"
    (is (== problem0028 669171001))))
