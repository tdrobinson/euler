(ns euler.page1.problem0024-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0024 :refer :all]))

(deftest problem0024-test
  (testing "Problem 0024 - Lexicographic permutations"
    (is (== problem0024 2783915460))))
