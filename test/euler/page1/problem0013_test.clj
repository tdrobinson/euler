(ns euler.page1.problem0013-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0013 :refer :all]))

(deftest problem0013-test
  (testing "Problem 0013 - Large sum"
    (is (== problem0013 5537376230))))
