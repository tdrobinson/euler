(ns euler.page1.problem0012-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0012 :refer :all]))

(deftest problem0012-test
  (testing "Problem 0012 - Highly divisible triangular number"
    (is (== problem0012 76576500))))
