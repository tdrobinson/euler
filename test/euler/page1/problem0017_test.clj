(ns euler.page1.problem0017-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0017 :refer :all]))

(deftest problem0017-test
  (testing "Problem 0017 - Number letter counts"
    (is (== problem0017 21124))))
