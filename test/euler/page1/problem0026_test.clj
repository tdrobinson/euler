(ns euler.page1.problem0026-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0026 :refer :all]))

(deftest problem0026-test
  (testing "Problem 0026 - Reciprocal cycles"
    (is (== problem0026 983))))
