(ns euler.page1.problem0020-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0020 :refer :all]))

(deftest problem0020-test
  (testing "Problem 0020 - Factorial digit sum"
    (is (== problem0020 648))))
