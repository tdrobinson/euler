(ns euler.page1.problem0009-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0009 :refer :all]))

(deftest problem0009-test
  (testing "Problem 0009 - Special Pythagorean triplet"
    (is (== problem0009 31875000))))
