(ns euler.page1.problem0029-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0029 :refer :all]))

(deftest problem0029-test
  (testing "Problem 0029 - Distinct powers"
    (is (== problem0029 9183))))
