(ns euler.page1.problem0021-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0021 :refer :all]))

(deftest problem0021-test
  (testing "Problem 0021 - Amicable numbers"
    (is (== problem0021 31626))))
