(ns euler.page1.problem0016-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0016 :refer :all]))

(deftest problem0016-test
  (testing "Problem 0016 - Power digit sum"
    (is (== problem0016 1366))))
