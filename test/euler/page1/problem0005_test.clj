(ns euler.page1.problem0005-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0005 :refer :all]))

(deftest problem0005-test
  (testing "Problem 0005 - Smallest multiple"
    (is (== problem0005 232792560))))
