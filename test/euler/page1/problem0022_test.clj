(ns euler.page1.problem0022-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0022 :refer :all]))

(deftest problem0022-test
  (testing "Problem 0022 - Names scores"
    (is (== problem0022 871198282))))
