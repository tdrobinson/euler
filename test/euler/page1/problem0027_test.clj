(ns euler.page1.problem0027-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0027 :refer :all]))

(deftest problem0027-test
  (testing "Problem 0027 - Quadratic primes"
    (is (== problem0027 -59231))))
