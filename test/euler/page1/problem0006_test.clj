(ns euler.page1.problem0006-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0006 :refer :all]))

(deftest problem0006-test
  (testing "Problem 0006 - Sum square difference"
    (is (== problem0006 25164150))))
