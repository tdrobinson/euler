(ns euler.page1.problem0001-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0001 :refer :all]))

(deftest problem0001-test
  (testing "Problem 0001 - Multiples of 3 and 5"
    (is (== problem0001 233168))))
