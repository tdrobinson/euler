(ns euler.page1.problem0025-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0025 :refer :all]))

(deftest problem0025-test
  (testing "Problem 0025 - 1000-digit Fibonacci number"
    (is (== problem0025 4782))))
