(ns euler.page1.problem0018-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0018 :refer :all]))

(deftest problem0018-test
  (testing "Problem 0018 - Maximum path sum I"
    (is (== problem0018 1074))))
