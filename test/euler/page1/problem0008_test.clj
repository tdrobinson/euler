(ns euler.page1.problem0008-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0008 :refer :all]))

(deftest problem0008-test
  (testing "Problem 0008 - Largest product in a series"
    (is (== problem0008 40824))))
