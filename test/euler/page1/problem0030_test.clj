(ns euler.page1.problem0030-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0030 :refer :all]))

(deftest problem0030-test
  (testing "Problem 0030 - Digit fifth powers"
    (is (== problem0030 443839))))
