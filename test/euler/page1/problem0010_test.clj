(ns euler.page1.problem0010-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0010 :refer :all]))

(deftest problem0010-test
  (testing "Problem 0010 - Summation of primes"
    (is (== problem0010 142913828922))))
