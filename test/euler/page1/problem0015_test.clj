(ns euler.page1.problem0015-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0015 :refer :all]))

(deftest problem0015-test
  (testing "Problem 0015 - Lattice paths"
    (is (== problem0015 137846528820))))
