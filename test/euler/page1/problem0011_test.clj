(ns euler.page1.problem0011-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0011 :refer :all]))

(deftest problem0011-test
  (testing "Problem 0011 - Largest product in a grid"
    (is (== problem0011 70600674))))
