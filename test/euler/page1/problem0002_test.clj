(ns euler.page1.problem0002-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0002 :refer :all]))

(deftest problem0002-test
  (testing "Problem 0002 - Even Fibonacci numbers"
    (is (== problem0002 4613732))))
