(ns euler.page1.problem0003-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0003 :refer :all]))

(deftest problem0003-test
  (testing "Problem 0003 - Largest prime factor"
    (is (== problem0003 6857))))
