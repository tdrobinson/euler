(ns euler.page1.problem0019-test
  (:require [clojure.test :refer :all]
            [euler.page1.problem0019 :refer :all]))

(deftest problem0019-test
  (testing "Problem 0019 - Counting Sundays"
    (is (== problem0019 171))))
