(defproject euler "0.1"
  :description "Implementations of Project Euler written in Clojure."
  :license {:name "DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE"
            :url "http://www.wtfpl.net/about/"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/math.combinatorics "0.0.7"]]
  :aot :all)
